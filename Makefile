dependency-install:
	docker pull postgres
	docker pull redis

dependency-start:
	docker run --name transactions-postgres -p 5432:5432 -d postgres
	docker run --name transactions-redis -p 6379:6379 -d redis

dependency-test:
	docker exec -it transactions-postgres psql -U postgres -c "CREATE DATABASE testdb ENCODING 'LATIN1' TEMPLATE template0 LC_COLLATE 'C' LC_CTYPE 'C';"
	docker exec -it transactions-postgres psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE testdb TO postgres;"

run-api:
	go run cmd/api/main.go -port=8080 -postgresURI=postgres://postgres:@localhost/postgres?sslmode=disable -redisURI=redis://localhost::6379

build:
	go build -o transactions-api ./cmd/api/main.go

build-linux:
	env GOOS=linux go build -o transactions-api ./cmd/api/main.go

test-all:
	go test ./... -v