module bitbucket.org/alexmourapb/transactions-api

go 1.12

require (
	github.com/Nhanderu/brdoc v1.1.2
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496
	github.com/aws/aws-sdk-go v1.29.30
	github.com/bsm/redis-lock v8.0.0+incompatible
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-pg/pg v8.0.6+incompatible
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/guregu/dynamo v1.6.1
	github.com/jinzhu/gorm v1.9.12
	github.com/rs/zerolog v1.18.0
	github.com/satori/go.uuid v1.2.0
	github.com/shopspring/decimal v0.0.0-20200227202807-02e2044944cc
	github.com/stretchr/testify v1.4.0
	mellium.im/sasl v0.2.1 // indirect
)
