package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/rs/zerolog"
	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	apiHandlers "bitbucket.org/alexmourapb/transactions-api/cmd/api/handlers"
	apiRouters "bitbucket.org/alexmourapb/transactions-api/cmd/api/routers"
	"bitbucket.org/alexmourapb/transactions-api/internal/customers"
	"bitbucket.org/alexmourapb/transactions-api/internal/helper/infrastructure/environment"
	apiLogger "bitbucket.org/alexmourapb/transactions-api/internal/helper/infrastructure/logger"
	"bitbucket.org/alexmourapb/transactions-api/internal/transactions"
)

const (
	defaultAppName = "Transactions-Api"
	defaultPort    = "8181"
)

var (
	port                    string
	appName                 string
	redisURI                string
	postgresURI             string
	transactionsEnvironment environment.Environment
)

func init() {
	flag.StringVar(&port, "port", os.Getenv("PORT"), "-port=8181")
	flag.StringVar(&appName, "appName", os.Getenv("APP_NAME"), "-appName=NameApp")
	flag.StringVar(&redisURI, "redisURI", os.Getenv("REDIS_URL"), "-redisURI=redis://localhost")
	flag.StringVar(&postgresURI, "postgresURI", os.Getenv("POSTGRES_URL"), "-postgresURI=postgres://postgres:@localhost/postgres?sslmode=disable")
}

func main() {

	flag.Parse()

	// Logger
	logger := apiLogger.Config(appName)
	//improvement logs
	logger = logger.Output(zerolog.NewConsoleWriter())

	logger.Info().Msg("Logger successfully initialized")

	logger.Info().Msg("[SERVER] :  Initializing APP_NAME")
	if appName == "" {
		_ = os.Setenv("APP_NAME", defaultAppName)
		appName = defaultAppName
		logger.Error().Msg("APP_NAME is empty, setting default: " + defaultAppName)
	}

	logger.Info().Msg("Initializing Port Number")
	if port == "" {
		_ = os.Setenv("PORT", defaultPort)
		port = defaultPort
		logger.Error().Msg("PORT is empty, setting default: " + defaultPort)
	}

	option, err := redis.ParseURL(redisURI)
	if err != nil {
		logger.Fatal().Err(err).Msg("can't parse redis uri")
		panic("Error Cache: " + err.Error())
	}

	cache := redis.NewClient(option)
	_, errCache := cache.Ping().Result()
	if errCache != nil {
		logger.Fatal().Err(errCache).Msg("can't initialize cache redis")
		panic("Error Cache: " + err.Error())
	}
	defer cache.Close()

	logger.Info().Msg("Initializing Postgres")
	db, err := gorm.Open("postgres", postgresURI)
	if err != nil {
		logger.Fatal().Err(err).Msg("can't initialize Postgres")
		panic("Error DB: " + err.Error())
	}
	defer db.Close()

	logger.Info().Msg("Initializing AutoMigrate Customers into Postgres")
	if err := db.AutoMigrate(&customers.Customer{}).Error; err != nil {
		logger.Error().Msg("can't AutoMigrate Customers into Postgres " + err.Error())
	}

	logger.Info().Msg("Initializing AutoMigrate CustomerAccounts into Postgres")
	if err := db.AutoMigrate(&customers.CustomerAccount{}).Error; err != nil {
		logger.Error().Msg("can't AutoMigrate CustomerAccounts into Postgres " + err.Error())
	}

	logger.Info().Msg("Initializing AutoMigrate Transactions into Postgres")
	if err := db.AutoMigrate(&transactions.Transaction{}).Error; err != nil {
		logger.Error().Msg("can't AutoMigrate Transactions into Postgres " + err.Error())
	}

	//Initial test data
	customerData := &customers.Customer{
		ID:          uuid.NewV4(),
		CPF:         "96558156415",
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountID, _ := uuid.FromString("5d544dee-3327-4f1a-8970-64e8928137a2")
	customerAccountData := &customers.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerData.ID,
		AvailableLimit: decimal.NewFromFloat(10000),
		Status:         "active",
	}

	db.Create(customerData)
	db.Create(customerAccountData)
	defer db.Delete(customerData)
	defer db.Delete(customerAccountData)

	h := &apiHandlers.Handler{DB: db, Cache: cache, Logger: &logger}
	routes := apiRouters.Router(h)

	ListenAndServe(&logger, port, time.Second * 10, routes)
}

// ListenAndServe with Graceful Shutdown
func ListenAndServe(log *zerolog.Logger, port string, timeout time.Duration, handler *chi.Mux) {
	if serverPort := os.Getenv("PORT"); serverPort != "" {
		port = serverPort
	}

	srv := http.Server{Addr: fmt.Sprintf(":%s", port), Handler: handler}

	c := make(chan os.Signal, 1)
	iddleConnections := make(chan struct{})
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		// sig is a ^C, handle it
		log.Info().Msg("[SERVER] : Shutdown...")

		// create context with timeout
		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()

		// start http shutdown
		if err := srv.Shutdown(ctx); err != nil {
			log.Error().Caller().Err(err).Msg("[SERVER] : Shutdown")
		}

		close(iddleConnections)
	}()

	log.Info().Str("PORT", port).Msg("[SERVER] : Listening...")
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatal().Caller().Err(err).Msg("[SERVER] : Listen and Serve fail")
	}

	log.Info().Msg("[SERVER] : waiting iddle connections...")
	<-iddleConnections
	log.Info().Msg("[SERVER] : Bye")
}
