package handlers

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
)

func Test_TransactionRequest_Bind(t *testing.T) {

	amount := decimal.NewFromFloat(10.0)
	amountZero := decimal.NewFromFloat(0)

	var tt = []struct {
		isValid bool
		request TransactionRequest
		message string
	}{
		{
			isValid: false,
			request: TransactionRequest{},
			message: "Empty request is not valid!",
		},
		{
			isValid: false,
			request: TransactionRequest{
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				Amount:               amount,
				NumberOfInstallments: 2,
			},
			message: "Empty cpf request is not valid!",
		},
		{
			isValid: false,
			request: TransactionRequest{
				CPF:                  "72535313099",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				Amount:               amount,
				NumberOfInstallments: 2,
			},
			message: "invalid cpf",
		},
		{
			isValid: false,
			request: TransactionRequest{
				CPF:                  "72535313022",
				Amount:               amount,
				NumberOfInstallments: 2,
			},
			message: "Empty account-id request is not valid!",
		},
		{
			isValid: false,
			request: TransactionRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c",
				Amount:               amount,
				NumberOfInstallments: 2,
			},
			message: "account-id is not a valid uuid",
		},
		{
			isValid: false,
			request: TransactionRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				NumberOfInstallments: 2,
			},
			message: "Empty amount request is not valid!",
		},
		{
			isValid: false,
			request: TransactionRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				Amount:               amountZero,
				NumberOfInstallments: 2,
			},
			message: "amount must be greater than zero",
		},
		{
			isValid: true,
			request: TransactionRequest{
				CPF:                  "72535313022",
				AccountID:            "57e65d60-58bf-406c-864c-f305b5927785",
				Amount:               amount,
				NumberOfInstallments: 2,
			},
			message: "",
		},
	}
	for _, tc := range tt {
		testName := tc.message
		t.Run(testName, func(t *testing.T) {

			err := tc.request.Bind(nil)
			if tc.isValid == true {
				assert.Nil(t, err)
			} else {
				assert.NotNil(t, err, tc.message)
			}
		})
	}
}

func Test_Handlers_Structs(t *testing.T) {

	type Fields struct {
		fieldName string
		fieldType string
		tags      string
	}

	var tt = []struct {
		modelName   string
		modelStruct interface{}
		fields      []Fields
	}{
		//Test Customer struct
		{"Transaction", &TransactionRequest{}, []Fields{
			{"CPF", "string", `json:"cpf" valid:"required"`},
			{"AccountID", "string", `json:"account-id" valid:"required"`},
			{"Amount", "decimal.Decimal", `json:"amount" valid:"required"`},
			{"NumberOfInstallments", "uint", `json:"number-of-installments" valid:"required"`}}},
	}
	for _, tc := range tt {

		reflectionElements := reflect.ValueOf(tc.modelStruct).Elem()
		for i, field := range tc.fields {

			testName := fmt.Sprintf("%s %s", tc.modelName, field.fieldName)
			t.Run(testName, func(t *testing.T) {

				// Check field name
				v := reflectionElements.FieldByName(field.fieldName)
				assert.True(t, v.IsValid(), "Field '%s' not found in the struct!", field.fieldName)

				// Check field type
				ts := fmt.Sprintf("%v", v.Type())
				assert.Equal(t, ts, field.fieldType,
					"Field '%s' should be of type '%s', but received '%s'",
					field.fieldName, field.fieldType, ts)

				// Check field tag
				tag := fmt.Sprintf("%s", reflectionElements.Type().Field(i).Tag)
				assert.Equal(t, tag, field.tags,
					"Field '%s' tag changed. Tag expected: %s",
					field.fieldName, tag)
			})

		}

		assert.Equal(t, len(tc.fields), reflectionElements.NumField(),
			"Found %d elements in struct (%s) while waiting for %d",
			len(tt), tc.modelName, reflectionElements.NumField())
	}
}
