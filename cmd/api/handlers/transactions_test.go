package handlers_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	apiHandlers "bitbucket.org/alexmourapb/transactions-api/cmd/api/handlers"
	apiRouters "bitbucket.org/alexmourapb/transactions-api/cmd/api/routers"
	"bitbucket.org/alexmourapb/transactions-api/internal/customers"
	"bitbucket.org/alexmourapb/transactions-api/internal/helper/infrastructure/cache"
	apiLogger "bitbucket.org/alexmourapb/transactions-api/internal/helper/infrastructure/logger"
	"bitbucket.org/alexmourapb/transactions-api/internal/helper/test"
	"bitbucket.org/alexmourapb/transactions-api/internal/transactions"
)

func Test_TransactionHandlerWithSuccess(t *testing.T) {

	logger := apiLogger.Config("transactions-api-test")

	cache := cache.NewRedisTest()
	defer cache.Close()
	defer cache.FlushAll()

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(customers.Customer{}.TableName())
	defer db.DropTable(customers.CustomerAccount{}.TableName())
	defer db.DropTable(transactions.Transaction{}.TableName())

	db.AutoMigrate(customers.Customer{})
	db.AutoMigrate(customers.CustomerAccount{})
	db.AutoMigrate(transactions.Transaction{})

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customers.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customers.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	db.Create(customerData)
	db.Create(customerAccountData)

	request := apiHandlers.TransactionRequest{
		CPF:                  cpf,
		AccountID:            customerAccountID.String(),
		Amount:               amount,
		NumberOfInstallments: 2,
	}

	payload := new(bytes.Buffer)
	err := json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/", payload)
	test.AssertOk(t, err)

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Cache: cache}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusCreated, w.Code)

	response := new(transactions.Transaction)
	decoder := json.NewDecoder(w.Body)
	err = decoder.Decode(&response)
	test.AssertOk(t, err)
	test.AssertEquals(t, response.Status, apiHandlers.TransactionStatusApproved)
}

func Test_GetTransactionHandlerWithSuccess(t *testing.T) {

	logger := apiLogger.Config("transactions-api-test")

	cache := cache.NewRedisTest()
	defer cache.Close()
	defer cache.FlushAll()

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(customers.Customer{}.TableName())
	defer db.DropTable(customers.CustomerAccount{}.TableName())
	defer db.DropTable(transactions.Transaction{}.TableName())

	db.AutoMigrate(customers.Customer{})
	db.AutoMigrate(customers.CustomerAccount{})
	db.AutoMigrate(transactions.Transaction{})

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customers.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customers.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	transactionData := &transactions.Transaction{
		ID:                   uuid.NewV4(),
		Amount:               amount,
		NumberOfInstallments: 1,
		CustomerID:           uuid.NewV4(),
		CustomerAccountID:    uuid.NewV4(),
		Status:               apiHandlers.TransactionStatusApproved,
	}

	db.Create(customerData)
	db.Create(customerAccountData)
	db.Create(transactionData)

	r, err := http.NewRequest("GET", "/"+transactionData.ID.String(), nil)
	test.AssertOk(t, err)

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Cache: cache}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusOK, w.Code)

	response := new(transactions.Transaction)
	decoder := json.NewDecoder(w.Body)
	err = decoder.Decode(&response)
	test.AssertOk(t, err)
	test.AssertEquals(t, response.ID, transactionData.ID)
}

func Test_ReprocessTransactionHandlerWithSuccess(t *testing.T) {

	logger := apiLogger.Config("transactions-api-test")

	cache := cache.NewRedisTest()
	defer cache.Close()
	defer cache.FlushAll()

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(customers.Customer{}.TableName())
	defer db.DropTable(customers.CustomerAccount{}.TableName())
	defer db.DropTable(transactions.Transaction{}.TableName())

	db.AutoMigrate(customers.Customer{})
	db.AutoMigrate(customers.CustomerAccount{})
	db.AutoMigrate(transactions.Transaction{})

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customers.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customers.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	transactionData := &transactions.Transaction{
		ID:                   uuid.NewV4(),
		Amount:               amount,
		NumberOfInstallments: 1,
		CustomerID:           customerID,
		CustomerAccountID:    customerAccountID,
		Status:               apiHandlers.TransactionStatusDenied,
		CreatedAt:            time.Date(1900, 1, 1, 0, 0, 0, 0, time.Local),
	}

	db.Create(customerData)
	db.Create(customerAccountData)
	db.Create(transactionData)

	r, err := http.NewRequest("POST", "/"+transactionData.ID.String()+"/reprocess", nil)
	test.AssertOk(t, err)

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Cache: cache}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusOK, w.Code)

	response := new(transactions.Transaction)
	decoder := json.NewDecoder(w.Body)
	err = decoder.Decode(&response)
	test.AssertOk(t, err)
	test.AssertEquals(t, response.Status, apiHandlers.TransactionStatusApproved)
}

func Test_TransactionHandlerWithError(t *testing.T) {

	logger := apiLogger.Config("transactions-api-test")

	cache := cache.NewRedisTest()
	defer cache.Close()
	defer cache.FlushAll()

	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(customers.Customer{}.TableName())
	defer db.DropTable(customers.CustomerAccount{}.TableName())
	defer db.DropTable(transactions.Transaction{}.TableName())

	db.AutoMigrate(customers.Customer{})
	db.AutoMigrate(customers.CustomerAccount{})
	db.AutoMigrate(transactions.Transaction{})

	customerID := uuid.NewV4()
	customerAccountID := uuid.NewV4()
	cpf := "02792449403"
	amount := decimal.NewFromFloat(10.0)

	customerData := &customers.Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	customerAccountData := &customers.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     customerID,
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	db.Create(customerData)
	db.Create(customerAccountData)

	request := apiHandlers.TransactionRequest{
		CPF:                  cpf,
		AccountID:            uuid.NewV4().String(),
		Amount:               amount,
		NumberOfInstallments: 2,
	}

	payload := new(bytes.Buffer)
	err := json.NewEncoder(payload).Encode(request)
	test.AssertOk(t, err)

	r, err := http.NewRequest("POST", "/", payload)
	test.AssertOk(t, err)

	r.Header.Add("Cache-Control", "no-cache")
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{Logger: &logger, DB: db, Cache: cache}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusBadRequest, w.Code)

	response := new(transactions.Transaction)
	decoder := json.NewDecoder(w.Body)
	err = decoder.Decode(&response)
	test.AssertOk(t, err)
}
