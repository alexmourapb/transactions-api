package handlers

import (
	"errors"
	"net/http"

	"github.com/Nhanderu/brdoc"
	"github.com/asaskevich/govalidator"
	"github.com/shopspring/decimal"
)

const (
	StatusActive = "active"

	TransactionStatusApproved = "approved"
	TransactionStatusDenied   = "denied"
)

type TransactionRequest struct {
	CPF                  string          `json:"cpf" valid:"required"`
	AccountID            string          `json:"account-id" valid:"required"`
	Amount               decimal.Decimal `json:"amount" valid:"required"`
	NumberOfInstallments uint            `json:"number-of-installments" valid:"required"`
}

func (t *TransactionRequest) Bind(r *http.Request) error {

	_, err := govalidator.ValidateStruct(*t)
	if err != nil {
		return err
	}

	if !brdoc.IsCPF(t.CPF) {
		return errors.New("invalid cpf")
	}

	if !govalidator.IsUUIDv4(t.AccountID) {
		return errors.New("account-id is not a valid uuid")
	}

	if t.Amount.LessThanOrEqual(decimal.Zero) {
		return errors.New("amount must be greater than zero")
	}

	return err
}

type AuthorizationResponse struct {
	ID                   string          `json:"id"`
	CPF                  string          `json:"cpf"`
	Amount               decimal.Decimal `json:"amount"`
	NumberOfInstallments uint            `json:"number-of-installments"`
	Status               string          `json:"status"`
}
