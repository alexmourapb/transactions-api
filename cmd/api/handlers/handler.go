package handlers

import (
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog"
)

type Handler struct {
	DB     *gorm.DB
	Cache  *redis.Client
	Logger *zerolog.Logger
}
