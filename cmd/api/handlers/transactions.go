package handlers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/asaskevich/govalidator"
	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"

	"bitbucket.org/alexmourapb/transactions-api/internal/customers"
	"bitbucket.org/alexmourapb/transactions-api/internal/fraud_prevention"
	helperHandlers "bitbucket.org/alexmourapb/transactions-api/internal/helper/handlers"
	"bitbucket.org/alexmourapb/transactions-api/internal/transactions"
)

func (h *Handler) TransactionHandler(w http.ResponseWriter, r *http.Request) {

	//Validate request
	request := &TransactionRequest{}
	errValidation := ValidateRequest(r, request)
	if errValidation != nil {
		h.Logger.Error().Err(errValidation).Msg("bad request")
		helperHandlers.Respond(w, r, http.StatusBadRequest, errValidation.Error())
		return
	}

	var wg sync.WaitGroup
	customer := &customers.Customer{}
	cAccount := &customers.CustomerAccount{}
	var errCustomer, errAccount error

	wg.Add(2)

	go func() {
		defer wg.Done()
		//Retrieve Customer Data
		customer, errCustomer = customers.GetCustomerByCPF(h.DB, request.CPF)
	}()

	go func() {
		defer wg.Done()
		//Retrieve Customer Account Data
		cAccount, errAccount = customers.GetCustomerAccountByID(h.DB, request.AccountID)
	}()

	wg.Wait()

	if errCustomer == gorm.ErrRecordNotFound {
		h.Logger.Error().Err(errCustomer).Msg("customer not found")
		helperHandlers.Respond(w, r, http.StatusBadRequest, errors.New("customer not found"))
		return
	}

	if errCustomer != nil {
		h.Logger.Error().Err(errCustomer).Msg("bad request")
		helperHandlers.Respond(w, r, http.StatusInternalServerError, errCustomer.Error())
		return
	}

	if errAccount == gorm.ErrRecordNotFound {
		h.Logger.Error().Err(errCustomer).Msg("customer account not found")
		helperHandlers.Respond(w, r, http.StatusBadRequest, errors.New("customer account not found"))
		return
	}

	if errAccount != nil {
		h.Logger.Error().Err(errAccount).Msg("bad request")
		helperHandlers.Respond(w, r, http.StatusInternalServerError, errAccount.Error())
		return
	}

	if customer.ID != cAccount.CustomerID {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "unrelated account with the customer")
		return
	}

	if customer.Status != StatusActive {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "customer status not active")
		return
	}

	if cAccount.Status != StatusActive {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "customer account status not active")
		return
	}

	if request.Amount.GreaterThan(cAccount.AvailableLimit) {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "insufficient funds")
		return
	}

	transactionInput := &transactions.Transaction{
		Amount:               request.Amount,
		NumberOfInstallments: request.NumberOfInstallments,
		CustomerID:           customer.ID,
		CustomerAccountID:    cAccount.ID,
	}

	errSecurity := fraud_prevention.ValidateTransaction(h.DB, transactionInput)
	if errSecurity != nil {
		transactionInput.Status = TransactionStatusDenied
		transactionInput.DeniedBy = errSecurity.Error()
		transactionOutput, err := transactions.InsertTransaction(h.DB, transactionInput)
		if err != nil {
			helperHandlers.Respond(w, r, http.StatusBadRequest, err)
			return
		}
		helperHandlers.Respond(w, r, http.StatusBadRequest, transactionOutput)
		return
	}

	_, err := customers.DebitCustomerAccountAvailableLimit(h.DB, cAccount.ID.String(), request.Amount)
	if err != nil {
		h.Logger.Error().Err(err)
		helperHandlers.Respond(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	transactionInput.Status = TransactionStatusApproved
	transactionOutput, err := transactions.InsertTransaction(h.DB, transactionInput)
	if err != nil {
		_, _ = customers.CreditCustomerAccountAvailableLimit(h.DB, cAccount.ID.String(), request.Amount)
		h.Logger.Error().Err(err)
		helperHandlers.Respond(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	helperHandlers.Respond(w, r, http.StatusCreated, transactionOutput)
}

func (h *Handler) GetTransactionHandler(w http.ResponseWriter, r *http.Request) {

	transactionID := chi.URLParam(r, "transactionID")
	if !govalidator.IsUUIDv4(transactionID) {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "invalid transactions-id")
		return
	}

	cached, err := h.Cache.Get(KeyGetDealers(transactionID)).Result()
	if err == nil {
		w.Header().Set("Content-Type", "application/json")
		_, _ = fmt.Fprintln(w, cached)
		return
	}

	transactionOutput, err := transactions.GetTransactionByID(h.DB, transactionID)
	if err == gorm.ErrRecordNotFound {
		h.Logger.Error().Err(err).Msg("transactions not found")
		helperHandlers.Respond(w, r, http.StatusNotFound, "transactions not found")
		return
	}

	if err != nil {
		h.Logger.Error().Err(err)
		helperHandlers.Respond(w, r, http.StatusInternalServerError, "transactions not found")
		return
	}

	serialized, err := json.Marshal(transactionOutput)
	err = h.Cache.Set(KeyGetDealers(transactionID), serialized, time.Minute*10).Err()
	if err != nil {
		h.Logger.Error().Err(err)
	}

	helperHandlers.Respond(w, r, http.StatusOK, transactionOutput)
}

//KeyGetTransaction ...
func KeyGetDealers(transactionID string) string {
	return "transaction" + transactionID
}

func (h *Handler) ReprocessTransactionHandler(w http.ResponseWriter, r *http.Request) {

	transactionID := chi.URLParam(r, "transactionID")
	if !govalidator.IsUUIDv4(transactionID) {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "invalid transactions-id")
		return
	}

	transactionOutput, err := transactions.GetTransactionByID(h.DB, transactionID)
	if err == gorm.ErrRecordNotFound {
		h.Logger.Error().Err(err).Msg("transactions not found")
		helperHandlers.Respond(w, r, http.StatusNotFound, "transactions not found")
		return
	}

	if err != nil {
		h.Logger.Error().Err(err)
		helperHandlers.Respond(w, r, http.StatusInternalServerError, "transactions not found")
		return
	}

	if transactionOutput.Status != TransactionStatusDenied {
		helperHandlers.Respond(w, r, http.StatusBadRequest, "ineligible transactions to reprocess")
		return
	}

	errSecurity := fraud_prevention.ValidateTransaction(h.DB, transactionOutput)
	if errSecurity != nil {
		transactionOutput.DeniedBy = errSecurity.Error()
		_, err := transactions.UpdateTransaction(h.DB, transactionID, TransactionStatusDenied, errSecurity.Error())
		if err != nil {
			helperHandlers.Respond(w, r, http.StatusBadRequest, err)
			return
		}
		helperHandlers.Respond(w, r, http.StatusBadRequest, transactionOutput)
		return
	}

	_, err = customers.DebitCustomerAccountAvailableLimit(h.DB, transactionOutput.CustomerAccountID.String(), transactionOutput.Amount)
	if err != nil {
		fmt.Println("MOPA MOPA", err.Error())
		h.Logger.Error().Err(err)
		helperHandlers.Respond(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	response, err := transactions.UpdateTransaction(h.DB, transactionOutput.ID.String(), TransactionStatusApproved, "")
	if err != nil {
		_, _ = customers.CreditCustomerAccountAvailableLimit(h.DB, transactionOutput.CustomerAccountID.String(), transactionOutput.Amount)
		h.Logger.Error().Err(err)
		helperHandlers.Respond(w, r, http.StatusInternalServerError, err.Error())
		return
	}

	h.Cache.Del(KeyGetDealers(transactionID))

	helperHandlers.Respond(w, r, http.StatusOK, response)
}
