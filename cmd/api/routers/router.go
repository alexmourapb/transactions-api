package routers

import (
	"net/http"

	"github.com/go-chi/chi"

	apiHandlers "bitbucket.org/alexmourapb/transactions-api/cmd/api/handlers"
	helperHandlers "bitbucket.org/alexmourapb/transactions-api/internal/helper/handlers"
	"bitbucket.org/alexmourapb/transactions-api/internal/helper/middleware"
)

//Router define the routes to the resources and methods
func Router(h *apiHandlers.Handler) *chi.Mux {

	router := chi.NewRouter()

	h.Logger.Info().Msg("Setup routes")

	//Check if the service is available.
	router.Get("/health", helperHandlers.HandleHealthCheck)

	router.Post("/", func(w http.ResponseWriter, r *http.Request) {
		middleware.Recover(middleware.RouteMonitoring(h.Logger, h.TransactionHandler)).ServeHTTP(w, r)
	})

	router.Get("/{transactionID}", func(w http.ResponseWriter, r *http.Request) {
		middleware.Recover(middleware.RouteMonitoring(h.Logger, h.GetTransactionHandler)).ServeHTTP(w, r)
	})

	router.Post("/{transactionID}/reprocess", func(w http.ResponseWriter, r *http.Request) {
		middleware.Recover(middleware.RouteMonitoring(h.Logger, h.ReprocessTransactionHandler)).ServeHTTP(w, r)
	})

	return router
}
