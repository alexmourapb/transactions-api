FROM golang:1.12
ENV GOPATH=/go
WORKDIR /go
RUN mkdir -p src/bitbucket.org/alexmourapb/transactions-api
COPY . /go/src/bitbucket.org/alexmourapb/transactions-api
WORKDIR /go/src/bitbucket.org/alexmourapb/transactions-api
RUN make build
FROM debian:buster-slim
RUN apt-get update && apt-get install ca-certificates -y && rm -rf /var/lib/apt/lists/*
WORKDIR /root/
COPY --from=0 /go/src/bitbucket.org/alexmourapb/transactions-api/transactions-api .
CMD ["./transactions-api"]