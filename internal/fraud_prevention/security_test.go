package fraud_prevention

import (
	"testing"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"bitbucket.org/alexmourapb/transactions-api/internal/customers"
	"bitbucket.org/alexmourapb/transactions-api/internal/helper/test"
	"bitbucket.org/alexmourapb/transactions-api/internal/transactions"
)

func Test_ValidateTransaction(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(customers.CustomerAccount{}.TableName())
	defer db.DropTable(transactions.Transaction{}.TableName())

	db.AutoMigrate(customers.CustomerAccount{})
	db.AutoMigrate(transactions.Transaction{})

	customerAccountID := uuid.NewV4()

	customerAccount := &customers.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     uuid.NewV4(),
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	transactionInput := &transactions.Transaction{
		Amount:               decimal.NewFromFloat(10),
		NumberOfInstallments: 1,
		CustomerID:           customerAccount.CustomerID,
		CustomerAccountID:    customerAccount.ID,
		Status:               "active",
	}

	db.Create(customerAccount)

	err := ValidateTransaction(db, transactionInput)
	test.AssertOk(t, err)
}

func Test_ValidateTransactionWithError(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(customers.CustomerAccount{}.TableName())
	defer db.DropTable(transactions.Transaction{}.TableName())

	db.AutoMigrate(customers.CustomerAccount{})
	db.AutoMigrate(transactions.Transaction{})

	customerAccountID := uuid.NewV4()

	customerAccount := &customers.CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     uuid.NewV4(),
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	transactionInput := &transactions.Transaction{
		ID:                   uuid.NewV4(),
		Amount:               decimal.NewFromFloat(10),
		NumberOfInstallments: 1,
		CustomerID:           customerAccount.CustomerID,
		CustomerAccountID:    customerAccount.ID,
		Status:               "active",
	}

	db.Create(transactionInput)
	db.Create(customerAccount)

	transactionInput.ID = uuid.NewV4()
	err := ValidateTransaction(db, transactionInput)
	test.AssertNotOk(t, err)
}
