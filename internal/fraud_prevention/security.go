package fraud_prevention

import (
	"errors"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"

	"bitbucket.org/alexmourapb/transactions-api/internal/transactions"
)

const (
	minimumIntervalBetweenTransactions float64 = 1
	maxTransactionsInInterval          int     = 3
	intervalBetweenTransactions        float64 = 5
)

func ValidateTransaction(db *gorm.DB, transactionInput *transactions.Transaction) error {

	transactions, err := transactions.GetAllTransactionsByCustomerID(db, transactionInput.CustomerID.String())
	if err != nil {
		return err
	}

	if len(transactions) > 0 {
		elapsed := time.Since(transactions[0].CreatedAt)
		if elapsed.Minutes() < minimumIntervalBetweenTransactions {
			return errors.New(fmt.Sprintf("transactions interval must be longer than %v minute", minimumIntervalBetweenTransactions))
		}

		if len(transactions) >= maxTransactionsInInterval {
			elapsed = time.Since(transactions[maxTransactionsInInterval-1].CreatedAt)
			if elapsed.Minutes() < intervalBetweenTransactions {
				return errors.New(fmt.Sprintf("no more than %v transactions are allowed in the %v minute interval", maxTransactionsInInterval, intervalBetweenTransactions))
			}
		}
	}

	return nil
}
