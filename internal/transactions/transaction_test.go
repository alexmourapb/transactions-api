package transactions

import (
	"testing"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"bitbucket.org/alexmourapb/transactions-api/internal/helper/test"
)

func Test_InsertTransaction(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(Transaction{}.TableName())

	db.AutoMigrate(Transaction{})

	transactionInput := &Transaction{
		Amount:               decimal.NewFromFloat(10),
		NumberOfInstallments: 1,
		CustomerID:           uuid.NewV4(),
		CustomerAccountID:    uuid.NewV4(),
		Status:               "approved",
	}

	_, err := InsertTransaction(db, transactionInput)
	test.AssertOk(t, err)
}

func Test_UpdateTransaction(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(Transaction{}.TableName())

	db.AutoMigrate(Transaction{})

	transactionInput := &Transaction{
		ID:                   uuid.NewV4(),
		Amount:               decimal.NewFromFloat(10),
		NumberOfInstallments: 1,
		CustomerID:           uuid.NewV4(),
		CustomerAccountID:    uuid.NewV4(),
		Status:               "approved",
	}

	db.Create(transactionInput)

	_, err := UpdateTransaction(db, transactionInput.ID.String(), "approved", "")
	test.AssertOk(t, err)
}

func Test_GetTransactionByID(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(Transaction{}.TableName())

	db.AutoMigrate(Transaction{})

	transactionInput := &Transaction{
		ID:                   uuid.NewV4(),
		Amount:               decimal.NewFromFloat(10),
		NumberOfInstallments: 1,
		CustomerID:           uuid.NewV4(),
		CustomerAccountID:    uuid.NewV4(),
		Status:               "approved",
	}

	db.Create(transactionInput)

	_, err := GetTransactionByID(db, transactionInput.ID.String())
	test.AssertOk(t, err)
}

func Test_GetAllTransactionsByCustomerID(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(Transaction{}.TableName())

	db.AutoMigrate(Transaction{})

	transactionInput := &Transaction{
		ID:                   uuid.NewV4(),
		Amount:               decimal.NewFromFloat(10),
		NumberOfInstallments: 1,
		CustomerID:           uuid.NewV4(),
		CustomerAccountID:    uuid.NewV4(),
		Status:               "approved",
	}

	db.Create(transactionInput)

	_, err := GetAllTransactionsByCustomerID(db, transactionInput.ID.String())
	test.AssertOk(t, err)
}
