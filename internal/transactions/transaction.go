package transactions

import (
	"github.com/jinzhu/gorm"
)

func InsertTransaction(db *gorm.DB, input *Transaction) (*Transaction, error) {

	input.BeforeInsert()

	if err := create(db, input); err != nil {
		return nil, err
	}

	return input, nil
}

func UpdateTransaction(db *gorm.DB, transactionID, status, deniedBy string) (*Transaction, error) {

	transaction, err := GetTransactionByID(db, transactionID)
	if err != nil {
		return nil, err
	}

	transaction.BeforeUpdate()
	transaction.Status = status
	transaction.DeniedBy = deniedBy

	if err := update(db, transaction); err != nil {
		return nil, err
	}

	return transaction, nil
}

func GetTransactionByID(db *gorm.DB, transactionID string) (*Transaction, error) {

	transaction := &Transaction{}

	if err := db.First(transaction, "id = ?", transactionID).Error; err != nil {
		return nil, err
	}

	return transaction, nil
}

func GetAllTransactionsByCustomerID(db *gorm.DB, customerID string) ([]Transaction, error) {

	transactions := make([]Transaction, 0)

	if err := db.Where("customer_id = ?", customerID).Find(&transactions).Order("created_at DESC").Error; err != nil {
		return transactions, err
	}

	return transactions, nil
}
