package transactions

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"bitbucket.org/alexmourapb/transactions-api/internal/customers"
)

const (
	transactionTableName = "transactions"
)

type Transaction struct {
	ID uuid.UUID `sql:",pk,type:uuid" json:"id"`

	Amount               decimal.Decimal `sql:"type:numeric" json:"amount"`
	NumberOfInstallments uint            `sql:",notnull" json:"number-of-installments"`

	// Customer
	Customer   *customers.Customer `json:"-"`
	CustomerID uuid.UUID           `sql:",type:uuid" json:"-"`

	// Customer Account
	CustomerAccount   *customers.CustomerAccount `json:"-"`
	CustomerAccountID uuid.UUID                  `sql:",type:uuid" json:"-"`

	Status   string `json:"status"`
	DeniedBy string `json:"denied-by,omitempty"`

	CreatedAt time.Time `json:"created-at"`
	UpdatedAt time.Time `json:"updated-at,omitempty"`
}

func (Transaction) TableName() string {
	return transactionTableName
}

func (t *Transaction) BeforeInsert() {

	t.ID = uuid.NewV4()
	t.CreatedAt = time.Now()
	return
}

func (t *Transaction) BeforeUpdate() {

	t.UpdatedAt = time.Now()
	return
}
