package transactions

import (
	"time"

	"github.com/jinzhu/gorm"
)

func create(db *gorm.DB, transaction *Transaction) error {

	if err := db.Create(transaction).Error; err != nil {
		return err
	}

	return nil
}

func update(db *gorm.DB, transaction *Transaction) error {

	transaction.UpdatedAt = time.Now()

	if err := db.Save(transaction).Error; err != nil {
		return err
	}

	return nil
}
