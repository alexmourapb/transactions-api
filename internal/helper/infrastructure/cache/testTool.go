package cache

import (
	"log"
	"os"

	"github.com/go-redis/redis"
)

//NewRedisTest ...
func NewRedisTest() *redis.Client {
	redisURI := os.Getenv("REDISTEST_URI")
	if redisURI == "" {
		redisURI = "redis://localhost"
	}

	cache, err := Connect(redisURI)
	if err != nil {
		log.Println("Error Cache: " + err.Error() + " " + redisURI)
		os.Exit(1)
	}

	return cache
}
