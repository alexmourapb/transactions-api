package cache

import (
	"errors"
	"time"

	redisLock "github.com/bsm/redis-lock"
	"github.com/go-redis/redis"
)

var (
	ErrorDistributedLock = errors.New("Distributed lock is fail")
)

//GetDistributedLock ...
func GetDistributedLock(cache *redis.Client, key string, lockTimeout time.Duration, retryDelay time.Duration) (*redisLock.Locker, error) {
	option := &redisLock.Options{LockTimeout: lockTimeout, RetryDelay: retryDelay}
	lock, err := redisLock.Obtain(cache, key, option)
	if err != nil {
		return nil, ErrorDistributedLock
	} else if lock == nil {
		return nil, ErrorDistributedLock
	}

	return lock, nil
}
