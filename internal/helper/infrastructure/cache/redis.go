package cache

import (
	"github.com/go-redis/redis"
)

func Connect(url string) (*redis.Client, error) {
	option, err := redis.ParseURL(url)
	if err != nil {
		return nil, err
	}

	cache := redis.NewClient(option)
	_, errPing := cache.Ping().Result()
	if errPing != nil {
		return nil, errPing
	}

	return cache, nil
}
