package logger

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"runtime/debug"
	"time"

	"github.com/go-chi/chi/middleware"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
)

func Middleware(l *zerolog.Logger) func(next http.Handler) http.Handler {

	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {

			logFields := getRequestFields(r)
			hlog.FromRequest(r).Info().Fields(logFields).Msg("request started")

			LogRequestParameters(r)

			ww := middleware.NewWrapResponseWriter(w, r.ProtoMajor)

			t1 := time.Now()
			defer func() {
				t2 := time.Now()

				if rec := recover(); rec != nil {
					logFields["stack"] = string(debug.Stack())
					logFields["panic"] = fmt.Sprintf("%+v", rec)

					hlog.FromRequest(r).Error().Fields(logFields).Msg("request panic")
					return
				}

				logFields["resp_status"] = ww.Status()
				logFields["resp_bytes_length"] = ww.BytesWritten()
				logFields["resp_elasped_ms"] = t2.Sub(t1)

				hlog.FromRequest(r).Info().Fields(logFields).Msg("request complete")
			}()

			next.ServeHTTP(ww, r)
		}
		return http.HandlerFunc(fn)
	}
}

func getRequestFields(r *http.Request) map[string]interface{} {
	logFields := map[string]interface{}{}

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		logFields["req_id"] = reqID
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	host := r.Host

	logFields["http_scheme"] = scheme
	logFields["http_proto"] = r.Proto
	logFields["http_method"] = r.Method

	logFields["remote_addr"] = r.RemoteAddr
	logFields["user_agent"] = r.UserAgent()

	logFields["uri"] = fmt.Sprintf("%s://%s%s", scheme, host, r.RequestURI)

	return logFields
}

func LogRequestParameters(r *http.Request) {
	requestBody := map[string]interface{}{}
	requestParameters := map[string]interface{}{}

	body, err := ioutil.ReadAll(r.Body)
	if err == nil {
		json.Unmarshal(body, &requestBody)
	}
	r.Body = ioutil.NopCloser(bytes.NewBuffer(body))

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		requestParameters["req_id"] = reqID
	}

	if len(requestBody) > 0 {
		requestParameters["body"] = requestBody
		hlog.FromRequest(r).Info().Fields(requestParameters).Msg("request parameters")
	}
}
