package environment

type Environment string

const (
	Test        = "test"
	Development = "development"
	Production  = "prod"
	Sandbox     = "sandbox"
)

func IsValid(env string) bool {
	return env == Test || env == Development || env == Production || env == Sandbox
}

func (e Environment) String() string {
	return string(e)
}

func FromString(env string) Environment {
	switch env {
	case "test":
		return Test
	case "development":
		return Development
	case "prod":
		return Production
	case "sandbox":
		return Sandbox
	}
	return Development
}
