package test

import (
	"github.com/jinzhu/gorm"
	"log"
	"os"
)

func NewDBTest() *gorm.DB {
	dbURI := os.Getenv("POSTGRESDBTEST_URL")
	if dbURI == "" {
		dbURI = "postgres://postgres:@localhost:5432/testdb?sslmode=disable"
	}

	db, err := gorm.Open("postgres", dbURI)
	if err != nil {
		log.Println("Error DB: " + err.Error() + " " + dbURI)
		os.Exit(1)
	}
	return db
}
