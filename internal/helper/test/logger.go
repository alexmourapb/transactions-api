package test

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
)

//NewLoggerTest ...
func NewLoggerTest(serviceName string) zerolog.Logger {

	logger := zerolog.New(os.Stdout).With().Timestamp().Logger()

	logger.Info().Msg(fmt.Sprintf("%s Service", serviceName))

	return logger
}
