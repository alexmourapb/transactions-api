package middleware

import (
	"net/http"

	"github.com/rs/zerolog"
)

//RouteMonitoring ...
func RouteMonitoring(logger *zerolog.Logger, fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		monitoringMsg := " PATH=" + r.Method + "_" + r.URL.String() + ""
		if logger != nil {
			logger.Info().Msg(monitoringMsg)
		}

		fn(w, r)
	}
}
