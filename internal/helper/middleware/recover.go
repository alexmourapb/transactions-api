package middleware

import (
	"log"
	"net/http"
	"runtime/debug"
)

//Recover ...
func Recover(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				log.Printf("recover %s: %s", err, debug.Stack())
				http.Error(w, http.StatusText(500), 500)
			}
		}()
		fn(w, r)
	}
}
