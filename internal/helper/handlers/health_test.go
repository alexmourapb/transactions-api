package handlers_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	apiHandlers "bitbucket.org/alexmourapb/transactions-api/cmd/api/handlers"
	apiRouters "bitbucket.org/alexmourapb/transactions-api/cmd/api/routers"
	apiLogger "bitbucket.org/alexmourapb/transactions-api/internal/helper/infrastructure/logger"
	"bitbucket.org/alexmourapb/transactions-api/internal/helper/test"
)

func TestHealthCheckHandle(t *testing.T) {

	r, err := http.NewRequest("GET", "/health", nil)
	if err != nil {
		t.Error(err.Error())
	}

	logger := apiLogger.Config("transactions-test")

	w := httptest.NewRecorder()

	h := &apiHandlers.Handler{DB: nil, Logger: &logger}

	apiRouters.Router(h).ServeHTTP(w, r)
	test.AssertEquals(t, http.StatusOK, w.Code)
}
