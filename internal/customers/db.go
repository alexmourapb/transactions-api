package customers

import (
	"time"

	"github.com/jinzhu/gorm"
)

func GetCustomerByCPF(db *gorm.DB, cpf string) (*Customer, error) {

	customer := &Customer{}

	if err := db.First(customer, "cpf = ?", cpf).Error; err != nil {
		return nil, err
	}

	return customer, nil
}

func GetCustomerAccountByID(db *gorm.DB, id string) (*CustomerAccount, error) {

	account := &CustomerAccount{}

	if err := db.First(account, "id = ?", id).Error; err != nil {
		return nil, err
	}

	return account, nil
}

func updateCustomerAccount(db *gorm.DB, cAccount *CustomerAccount) error {

	cAccount.UpdatedAt = time.Now()

	if err := db.Save(cAccount).Error; err != nil {
		return err
	}

	return nil
}
