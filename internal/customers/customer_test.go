package customers

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"bitbucket.org/alexmourapb/transactions-api/internal/helper/test"
)

func Test_CreditCustomerAccountAvailableLimit(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(CustomerAccount{}.TableName())

	db.AutoMigrate(CustomerAccount{})

	customerAccountID := uuid.NewV4()

	customerAccount := &CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     uuid.NewV4(),
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	db.Create(customerAccount)

	output, err := CreditCustomerAccountAvailableLimit(db, customerAccountID.String(), decimal.NewFromFloat(10))
	test.AssertOk(t, err)

	if !output.AvailableLimit.Equal(customerAccount.AvailableLimit.Add(decimal.NewFromFloat(10))) {
		t.Fatal("customerAccountOutput.AvailableLimit != customerAccount.AvailableLimit")
	}
}

func Test_DebitCustomerAccountAvailableLimit(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(CustomerAccount{}.TableName())

	db.AutoMigrate(CustomerAccount{})

	customerAccountID := uuid.NewV4()

	customerAccount := &CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     uuid.NewV4(),
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	db.Create(customerAccount)

	output, err := DebitCustomerAccountAvailableLimit(db, customerAccountID.String(), decimal.NewFromFloat(10))
	test.AssertOk(t, err)

	if !output.AvailableLimit.Equal(customerAccount.AvailableLimit.Sub(decimal.NewFromFloat(10))) {
		t.Fatal("customerAccountOutput.AvailableLimit != customerAccount.AvailableLimit")
	}
}
