package customers

import (
	"testing"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"

	"bitbucket.org/alexmourapb/transactions-api/internal/helper/test"
)

func Test_GetCustomerByCPF(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(Customer{}.TableName())

	db.AutoMigrate(Customer{})

	customerID := uuid.NewV4()
	cpf := "02792449403"

	customer := &Customer{
		ID:          customerID,
		CPF:         cpf,
		Name:        "Customer Test Name",
		PhoneNumber: "83999998877",
		Status:      "active",
	}

	db.Create(customer)

	customerOutput, err := GetCustomerByCPF(db, cpf)
	if err != nil {
		t.Fatal(err.Error())
	}

	if customerOutput.ID != customerID {
		t.Fatal("customerOutput.ID != customerID")
	}
}

func Test_GetCustomerAccountByID(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(CustomerAccount{}.TableName())

	db.AutoMigrate(CustomerAccount{})

	customerAccountID := uuid.NewV4()

	customerAccount := &CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     uuid.NewV4(),
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	db.Create(customerAccount)

	customerAccountOutput, err := GetCustomerAccountByID(db, customerAccountID.String())
	if err != nil {
		t.Fatal(err.Error())
	}

	if customerAccountOutput.ID != customerAccountID {
		t.Fatal("customerAccountOutput.ExternalID != externalID")
	}
}

func Test_updateCustomerAccount(t *testing.T) {
	db := test.NewDBTest()
	defer db.Close()
	defer db.DropTable(CustomerAccount{}.TableName())

	db.AutoMigrate(CustomerAccount{})

	customerAccountID := uuid.NewV4()

	customerAccount := &CustomerAccount{
		ID:             customerAccountID,
		CustomerID:     uuid.NewV4(),
		AvailableLimit: decimal.NewFromFloat(1000),
		Status:         "active",
	}

	db.Create(customerAccount)

	customerAccountOutput, err := GetCustomerAccountByID(db, customerAccountID.String())
	if err != nil {
		t.Fatal(err.Error())
	}

	customerAccountOutput.AvailableLimit = customerAccountOutput.AvailableLimit.Sub(decimal.NewFromFloat(10))
	err = updateCustomerAccount(db, customerAccountOutput)
	if err != nil {
		t.Fatal(err.Error())
	}

	output, err := GetCustomerAccountByID(db, customerAccountID.String())
	if err != nil {
		t.Fatal(err.Error())
	}

	if !output.AvailableLimit.Equal(customerAccountOutput.AvailableLimit) {
		t.Fatal("customerAccountOutput.AvailableLimit != customerAccountOutput.AvailableLimit)")
	}
}
