package customers

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Customer_Structs(t *testing.T) {

	type Fields struct {
		fieldName string
		fieldType string
		tags      string
	}

	var tt = []struct {
		modelName   string
		modelStruct interface{}
		fields      []Fields
	}{
		//Test Customer struct
		{"Customer", &Customer{}, []Fields{
			{"ID", "uuid.UUID", `sql:",pk,type:uuid" json:"id"`},
			{"CPF", "string", `sql:",notnull,unique" json:"cpf"`},
			{"Name", "string", `json:"name"`},
			{"PhoneNumber", "string", `json:"phone-number"`},
			{"Status", "string", `json:"status"`}}},

		//Test CustomerAccount struct
		{"CustomerAccount", &CustomerAccount{}, []Fields{
			{"ID", "uuid.UUID", `sql:",pk,type:uuid" json:"id"`},
			{"Customer", "*customers.Customer", `json:"-"`},
			{"CustomerID", "uuid.UUID", `sql:",type:uuid" json:"-"`},
			{"AvailableLimit", "decimal.Decimal", `sql:"type:numeric" json:"available-limit"`},
			{"Status", "string", `json:"status"`},
			{"CreatedAt", "time.Time", `json:"-"`},
			{"UpdatedAt", "time.Time", `json:"-"`}}},
	}
	for _, tc := range tt {

		reflectionElements := reflect.ValueOf(tc.modelStruct).Elem()
		for i, field := range tc.fields {

			testName := fmt.Sprintf("%s %s", tc.modelName, field.fieldName)
			t.Run(testName, func(t *testing.T) {

				// Check field name
				v := reflectionElements.FieldByName(field.fieldName)
				assert.True(t, v.IsValid(), "Field '%s' not found in the struct!", field.fieldName)

				// Check field type
				ts := fmt.Sprintf("%v", v.Type())
				assert.Equal(t, ts, field.fieldType,
					"Field '%s' should be of type '%s', but received '%s'",
					field.fieldName, field.fieldType, ts)

				// Check field tag
				tag := fmt.Sprintf("%s", reflectionElements.Type().Field(i).Tag)
				assert.Equal(t, tag, field.tags,
					"Field '%s' tag changed. Tag expected: %s",
					field.fieldName, tag)
			})

		}

		assert.Equal(t, len(tc.fields), reflectionElements.NumField(),
			"Found %d elements in struct (%s) while waiting for %d",
			len(tt), tc.modelName, reflectionElements.NumField())
	}
}
