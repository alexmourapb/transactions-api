package customers

import (
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/shopspring/decimal"
)

const (
	customerTableName        = "customers"
	customerAccountTableName = "customer_accounts"
)

type Customer struct {
	ID          uuid.UUID `sql:",pk,type:uuid" json:"id"`
	CPF         string    `sql:",notnull,unique" json:"cpf"`
	Name        string    `json:"name"`
	PhoneNumber string    `json:"phone-number"`
	Status      string    `json:"status"`
}

func (Customer) TableName() string {
	return customerTableName
}

type CustomerAccount struct {
	ID             uuid.UUID       `sql:",pk,type:uuid" json:"id"`
	Customer       *Customer       `json:"-"`
	CustomerID     uuid.UUID       `sql:",type:uuid" json:"-"`
	AvailableLimit decimal.Decimal `sql:"type:numeric" json:"available-limit"`
	Status         string          `json:"status"`

	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
}

func (CustomerAccount) TableName() string {
	return customerAccountTableName
}

func (c *CustomerAccount) BeforeUpdate() {

	c.UpdatedAt = time.Now()
	return
}
