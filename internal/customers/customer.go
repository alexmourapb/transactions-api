package customers

import (
	"github.com/jinzhu/gorm"
	"github.com/shopspring/decimal"
)

func DebitCustomerAccountAvailableLimit(db *gorm.DB, cAccountID string, amount decimal.Decimal) (*CustomerAccount, error) {

	cAccount, err := GetCustomerAccountByID(db, cAccountID)
	if err != nil {
		return nil, err
	}

	cAccount.BeforeUpdate()
	cAccount.AvailableLimit = cAccount.AvailableLimit.Sub(amount)

	if err := updateCustomerAccount(db, cAccount); err != nil {
		return nil, err
	}

	return cAccount, nil
}

func CreditCustomerAccountAvailableLimit(db *gorm.DB, cAccountID string, amount decimal.Decimal) (*CustomerAccount, error) {

	cAccount, err := GetCustomerAccountByID(db, cAccountID)
	if err != nil {
		return nil, err
	}

	cAccount.BeforeUpdate()
	cAccount.AvailableLimit = cAccount.AvailableLimit.Add(amount)

	if err := updateCustomerAccount(db, cAccount); err != nil {
		return nil, err
	}

	return cAccount, nil
}
