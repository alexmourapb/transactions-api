# transactions-api

You must already have the docker installed, ok?

Running the application first time
------------------
```
make dependency-install
make dependency-start
make run-api
make run-worker
```

Running the test first time
------------------
```
make dependency-install
make dependency-start
make dependency-test
make test-all
```

Consuming the API
------------------

Request:
```
curl -X POST \
  http://187.19.204.244:8181/ \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
	"cpf":"96558156415",
	"account-id":"57e65d60-58bf-406c-864c-f305b5927785",
	"amount": 10.0,
	"number-of-installments": 4,
}'
```

Response:
```
{
    "id": "c2cc95c1-a991-4b94-86a9-6e3b1c45d024",
    "amount": "10.1",
    "number-of-installments": 2,
    "status": "approved",
    "created-at": "2020-03-24T21:46:14.600404Z",
}
```

Valid Payload
------------------

Request 01:
```
{
	"cpf":"96558156415",
	"account-id":"5d544dee-3327-4f1a-8970-64e8928137a2",
	"amount": 20.10,
	"number-of-installments": 2,
}
```
